package com.swissquote.lauzhack.evolution.api;

import java.math.BigDecimal;

public class Price {
	/**
	 * Bought currency
	 */
	public final Currency base;

	/**
	 * Currency that is being sold (either by the client, or by the bank when at your request)
	 */
	public final Currency term;

	/**
	 * Currency that is being sold (either by the client, or by the bank when at your request)
	 */
	public final BigDecimal rate;
	public final BigDecimal markup;

	public Price(Currency base, Currency term, BigDecimal rate, BigDecimal markup) {
		this.base = base;
		this.term = term;
		this.rate = rate;
		this.markup = markup;
	}
}
