package com.swissquote.lauzhack.evolution.api;

/**
 * Implement this interface to create your BBook engine
 */
public interface BBook {

	/**
	 * Called before the market starts. Good place to prepare before the fight !
	 */
	void onInit();

	/**
	 * Called each time a trade is made by a client
	 * Beware of concurrent accesses here
	 * @param trade
	 */
	void onTrade(Trade trade);

	/**
	 * Called each time a price is published on the market
	 * @param price
	 */
	void onPrice(Price price);

	/**
	 * Simply used to get a hold on the bank reference.
	 * Helpful to order trades.
	 * @param bank
	 */
	void setBank(Bank bank);

}
