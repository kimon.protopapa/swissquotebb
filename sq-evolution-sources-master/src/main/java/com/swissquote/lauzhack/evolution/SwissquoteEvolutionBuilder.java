package com.swissquote.lauzhack.evolution;

import com.swissquote.lauzhack.evolution.api.BBook;
import com.swissquote.lauzhack.evolution.api.MarketProfile;
import com.swissquote.lauzhack.evolution.api.SwissquoteEvolution;

/**
 * Build for the SwissquoteEvolution app runner
 */
public class SwissquoteEvolutionBuilder {

	/**
	 * Please fill in your team name (used during file generation)
	 * @param team
	 * @return
	 */
	public SwissquoteEvolutionBuilder team(String team) {
		this.team = team;
		return this;
	}

	/**
	 * Used to initialized the randomization
	 * @param seed
	 * @return
	 */
	public SwissquoteEvolutionBuilder seed(Integer seed) {
		this.seed = seed;
		return this;
	}

	/**
	 * Your BBook engine to connect with the bank
	 * @param bBook
	 * @return
	 */
	public SwissquoteEvolutionBuilder bBook(BBook bBook) {
		this.bBook = bBook;
		return this;
	}

	/**
	 * Market profile to be used. Check the enum for more details.
	 * RANDOM will be used for the trial
	 * @param profile
	 * @return
	 */
	public SwissquoteEvolutionBuilder profile(MarketProfile profile) {
		this.profile = profile;
		return this;
	}

	/**
	 * Length (in market moves) of the scenario.
	 * 5000 will be used for the trial
	 */
	public SwissquoteEvolutionBuilder steps(Integer steps) {
		this.steps = steps;
		return this;
	}

	/**
	 * Time (in ms) between market increment.
	 * 1 will be used for the trial
	 */
	public SwissquoteEvolutionBuilder interval(Integer interval) {
		this.interval = interval;
		return this;
	}

	/**
	 * Where the result file will be stored.
	 * You can upload the result to : https://astat.github.io/sq-evolution-viewer/ for a nice preview
	 */
	public SwissquoteEvolutionBuilder filePath(String filePath) {
		this.filePath = filePath;
		return this;
	}

	private String filePath;
	private String team;
	private Integer seed;
	private BBook bBook;
	private MarketProfile profile;
	private Integer steps;
	private Integer interval;

	public SwissquoteEvolution build() {
		return null;
	}
}
