package com.swissquote.lauzhack.evolution.api;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Trade {

	/**
	 * Currency that is being bought (either by the client, or by the bank when at your request)
	 */
	public final Currency base;

	/**
	 * Currency that it is being exchanged from (either by the client, or by the bank when at your request)
	 */
	public final Currency term;

	/**
	 * How much is traded, expressed in base currency
	 */
	public final BigDecimal quantity;

	/**
	 * Create a Trade
	 * One of the currency must **always** be CHF
	 * The quantity must be positive
	 * @param base
	 * @param term
	 * @param quantity
	 */
	public Trade(Currency base, Currency term, BigDecimal quantity) {
		if (quantity.compareTo(BigDecimal.ZERO) <= 0) {
			throw new IllegalArgumentException("Trade with null or negative quantity are forbidden");
		}
		if (base != Currency.CHF && term != Currency.CHF) {
			throw new IllegalArgumentException("All trades should pass through CHF intermediary");
		}
		this.base = base;
		this.term = term;
		this.quantity = quantity.setScale(2, RoundingMode.HALF_EVEN);
	}
}
