package com.swissquote.lauzhack.evolution.api;

/**
 * This represent your bank, and allow you to order trades on the market
 */
public interface Bank {

	/**
	 * Ask the bank to buy this trade on the market
	 * @param trade
	 */
	void buy(Trade trade);
}
