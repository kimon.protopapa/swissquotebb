package com.swissquote.lauzhack.evolution.api;

public interface SwissquoteEvolution extends Runnable {

	/**
	 * Return the books of the bank (yes, everything is stored, it's a bank after all..)
	 * This is used to generate the output file
	 * @return
	 */
	String logBook();
}
