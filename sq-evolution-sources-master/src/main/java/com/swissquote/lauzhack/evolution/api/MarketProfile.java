package com.swissquote.lauzhack.evolution.api;

public enum MarketProfile {
	/**
	 * Let's go slow. Don't go too hard
	 */
	SOMETHING,
	/**
	 * Quiet market, quiet day.. Life is nice
	 */
	POC,
	/**
	 * Not that is something
	 */
	IT_WORKS,
	/**
	 * Get a taste of the challenge
	 */
	STARTUP,
	/**
	 * What you gonna do ?
	 */
	UNICORN,
	/**
	 * Your engine should react properly without know the current profile
	 */
	RANDOM
}
