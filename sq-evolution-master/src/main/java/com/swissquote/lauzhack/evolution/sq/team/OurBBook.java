package com.swissquote.lauzhack.evolution.sq.team;

import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.util.*;
import java.math.BigDecimal;
import com.swissquote.lauzhack.evolution.api.BBook;
import com.swissquote.lauzhack.evolution.api.Bank;
import com.swissquote.lauzhack.evolution.api.Currency;
import com.swissquote.lauzhack.evolution.api.Price;
import com.swissquote.lauzhack.evolution.api.Trade;
import java.io.FileWriter;

/**
 * This is a very simple example of implementation.
 * This class can be completely discarded.
 */
public class OurBBook implements BBook {

	// Save a reference to the bank in order to pass orders
	private Bank bank;
	final private int N_PRICES = 500;
	private double euroAcc = 1000000;
	private double usdAcc  = 1000000;
	private double gbpAcc  = 1000000;
	private double jpyACC  = 1000000;
	private Price currPrice;
	public ArrayList<Price> EUR_prices = new ArrayList<>(N_PRICES);
	public ArrayList<Price> USD_prices = new ArrayList<>(N_PRICES);
	public ArrayList<Price> JPY_prices = new ArrayList<>(N_PRICES);
	public ArrayList<Price> GBP_prices = new ArrayList<>(N_PRICES);

	
	public void writeJSON(ArrayList<Price> al, String path){
		try {
			FileWriter Fwriter = new FileWriter(path);
			PrintWriter writer = new PrintWriter(Fwriter);
			writer.print("{ "+ "\"" + "prices_"+ al.get(0).base + "_"+ al.get(0).term + "\"" + ": ");
			for(Price p : al) {
				writer.print( p.rate.doubleValue() +  ", ");
			}
			writer.print("}");
			writer.close();
		}catch(Exception e){

		}

	}

	@Override
	public void onInit() {
		// Start by buying some cash. Don't search for more logic here: numbers are just random..
		bank.buy(new Trade(Currency.EUR, Currency.CHF, new BigDecimal(1000000)));
		bank.buy(new Trade(Currency.JPY, Currency.CHF, new BigDecimal(1000000)));
		bank.buy(new Trade(Currency.USD, Currency.CHF, new BigDecimal(1000000)));
		bank.buy(new Trade(Currency.GBP, Currency.CHF, new BigDecimal(1000000)));
	}

	@Override
	public void onTrade(Trade trade) {
		// It would certainly be wise to store the available amount per currency..
		Currency currentBase = trade.base;

		switch (trade.base) {
			case EUR:
				this.euroAcc -= trade.quantity.doubleValue();
				break;
			case GBP:
				this.gbpAcc -= trade.quantity.doubleValue();
				break;
			case JPY:
				this.jpyACC -= trade.quantity.doubleValue();
				break;
			case USD:
				this.usdAcc -= trade.quantity.doubleValue();
				break;
			case CHF:
				switch (trade.term) {
					case EUR:
						currPrice = EUR_prices.get(EUR_prices.size() - 1);
						this.euroAcc += trade.quantity.doubleValue() / (currPrice.rate.doubleValue() * (1 - currPrice.markup.doubleValue()));
						break;
					case GBP:
						currPrice = GBP_prices.get(GBP_prices.size() - 1);
						this.gbpAcc += trade.quantity.doubleValue() / (currPrice.rate.doubleValue() * (1 - currPrice.markup.doubleValue()));
						break;
					case JPY:
						currPrice = JPY_prices.get(JPY_prices.size() - 1);
						this.jpyACC += trade.quantity.doubleValue() / (currPrice.rate.doubleValue() * (1 - currPrice.markup.doubleValue()));
						break;
					case USD:
						currPrice = USD_prices.get(USD_prices.size() - 1);
						this.usdAcc += trade.quantity.doubleValue() / (currPrice.rate.doubleValue() * (1 - currPrice.markup.doubleValue()));
						break;
				}
		}
//		System.out.println(" --EURO--- " + euroAcc);
//		System.out.println(" --GBP--- " + gbpAcc);
//		System.out.println(" --JPY--- " + jpyACC);
//		System.out.println(" --USD--- " + usdAcc);


	}

	public double kadanesubarray(double[] array){
		double local_min = 0;
		double global_min = Double.MAX_VALUE;
		for (int i = 0; i < array.length; i++) {
			local_min = Double.min(array[i], array[i] + local_min);
			if (local_min < global_min)
				global_min = local_min;
		}
		return global_min;
	}

	@Override
	public void onPrice(Price price) {
		if (price.base == Currency.EUR) EUR_prices.add(price);
		else if (price.base == Currency.USD) USD_prices.add(price);
		else if (price.base == Currency.JPY) JPY_prices.add(price);
		else if (price.base == Currency.GBP) GBP_prices.add(price);
	}

	@Override
	public void setBank(Bank bank) {
		this.bank = bank;
	}
}
