package com.swissquote.lauzhack.evolution.sq.team;

import com.swissquote.lauzhack.evolution.SwissquoteEvolutionBuilder;
import com.swissquote.lauzhack.evolution.api.MarketProfile;
import com.swissquote.lauzhack.evolution.api.SwissquoteEvolution;

import java.io.PrintStream;
import java.io.PrintWriter;

public class App {

	/**
	 * This is the starter for the application.
	 * You can keep this one, or create your own (using any Framework)
	 * As long as you run a SwissquoteEvolution
	 */
	public static void main(String[] args) {
		// Instantiate our BBook
		OurBBook ourBBook = new OurBBook();

		// Create the application runner
		SwissquoteEvolution app = new SwissquoteEvolutionBuilder().
				profile(MarketProfile.SOMETHING).
				seed(1).
				team("Prom1").
				bBook(ourBBook).
				filePath("../sq-evolution-master/examples/").
				interval(1).
				steps(5000).
				build();

		// Let's go !
		app.run();
		double[] arr = {1.0, -3, 2, -4, 57, -66, -5 , 100};
		System.out.println(ourBBook.kadanesubarray(arr));

		//ourBBook.writeJSON(ourBBook.EUR_prices,"prices01.JSON");
		//ourBBook.writeJSON(ourBBook.JPY_prices,"prices02.JSON");
		//ourBBook.writeJSON(ourBBook.GBP_prices,"prices03.JSON");
		//ourBBook.writeJSON(ourBBook.USD_prices,"prices04.JSON");

		// Display the result as JSON in console (also available in the file at "Path")

		System.out.println(app.logBook());
	}

}
