import json as js
import numpy as np
import pandas as pd
import keras
from keras import layers
import matplotlib.pyplot as plt

with open("three.json") as file:
    temp = js.loads(file.read())

WINDOW = 1
OUTPUT_DIM = 10


def make_seq(data, window=WINDOW, out_dims=OUTPUT_DIM):
    N = data.shape[0]
    size = N - window - OUTPUT_DIM
    X, y = np.ones(shape=(size, window, 1), dtype=np.float), np.ones(shape=(size, out_dims), dtype=np.float)
    for i in range(0, size):
        X[i] = np.reshape(data[i: window + i], (window, 1))
        y[i] = np.reshape(data[window + i: window + i + out_dims], (10,))

    return X, y


df = pd.DataFrame.from_dict(temp["prices"])
data = []
i = 0
#joins a regularized version of each currency for model building
for col in iter(df.columns):
    arr = np.array(df[col])
    arr = (arr - arr.min(axis=0)) / (arr.max(axis=0) - arr.min(axis=0))
    data.append(arr)


data = np.concatenate(data, axis=0)


def create_model(N_hidden = 20):
    model = keras.Sequential()
    model.add(layers.LSTM(N_hidden, input_shape=(WINDOW, 1), implementation=1, unroll=True))
    model.add(layers.Dense(OUTPUT_DIM))
    model.summary()

    return model


def train_model(model, train, test, epochs=100, initial_epoch=0):
    err_train, err_test = [], []
    X_train, y_train = make_seq(train)
    model.compile(keras.optimizers.SGD(), loss="mean_squared_error", metrics=["mean_squared_error"])
    for i in range(0, epochs):
        history = model.fit(x=X_train, y=y_train, epochs=i + 1 + initial_epoch, verbose=2, initial_epoch=i + initial_epoch)
        err_train.append(history.history["loss"][0])
        J, _, _ = test_model(model, test)
        err_test.append(J[0])
    return model, err_train, err_test


def test_model(model, test):
    X_test, y_test = make_seq(test)
    yhat = model.predict(X_test, verbose=0)
    N = y_test.shape[0]
    return ((yhat - y_test)**2).sum(axis=0), yhat, y_test


def load_model(path):
    return keras.models.load_model(path)


def save_model(model, path):
    keras.models.save_model(model, path)


train = data[:int(3 * data.shape[0] / 4)]
test = data[int(3 * data.shape[0] / 4):]

model, J_train, J_test = train_model(create_model(), train, test, epochs=10)
save_model(model, "1_in_10_out.h5")
_, yhat, y_test = test_model(model, test)

plt.plot(range(0, yhat.shape[0]), list(y[0] for y in yhat), color="orange")
plt.plot(range(0, y_test.shape[0]), list(y[0] for y in y_test), color="blue")
plt.title("Three hidden layers with regularization")
plt.show()

