import json as js
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import keras
from keras import layers

WINDOW = 10
n_nodes = 5
folds = 5

with open("out.json") as all:
    mydict = js.loads(all.read())

prices = mydict["prices"]
N = len(prices["CHF"])

df_prices = pd.DataFrame.from_dict(prices)

data_GBP = df_prices["GBP"].to_numpy(dtype=np.float)
data_max = data_GBP.max(axis=0)
data_min = data_GBP.min(axis=0)
train = data_GBP[: int(3 * N / 4)]
test = data_GBP[int(3 * N / 4):]


model = keras.models.load_model("model.h5")

def make_sequences(dataset, window=WINDOW):
    X, y = np.ones(shape=(dataset.shape[0] - WINDOW - 1, WINDOW, 1), dtype=np.float),\
           np.ones(shape=(dataset.shape[0] - WINDOW - 1, 1), dtype=np.float)
    for i in range(0, dataset.shape[0] - WINDOW - 2):
        X[i] = np.reshape(dataset[i: i + WINDOW], (WINDOW, 1))
        y[i] = dataset[i + WINDOW]
    return X, y

X, y = make_sequences(train)

#model.fit(X, y, epochs=50, verbose=1, shuffle=True)
model.save("model.h5")

X, y = make_sequences(test)

yhat = model.predict(X, verbose=1)
yhat = yhat * (data_max - data_min) + data_min
print(yhat[:100])

plt.plot(range(0, len(yhat)), yhat, color="orange")
plt.plot(range(0, len(yhat)), list(y), color="blue")
plt.show()
